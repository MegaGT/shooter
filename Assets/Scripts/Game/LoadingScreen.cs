﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] private Canvas _screenCanvas;
        [SerializeField] private Image _progressImage;

        //Показывает экран
        public void Show()
        {
            _progressImage.fillAmount = 0.0f;
            _screenCanvas.enabled = true;
        }

        public void UpdateLoadProgress(float progress)
        {
            _progressImage.fillAmount = progress;
        }

        //Скрывает экран
        public void Hide()
        {
            _screenCanvas.enabled = false;
        }

    }
}