using Game;
using Game.Constants;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts.Game
{
    public class GameInstance : MonoBehaviour
    {
        [SerializeField] private GameConstants _gameConstants;
        [SerializeField] private LoadingScreen _loadingScreen;

        private static GameInstance _instance;


        private void Awake()
        {
            if(_instance != null)
            {
                Destroy(gameObject);
                return;
            }


            DontDestroyOnLoad(gameObject);

            Application.targetFrameRate = _gameConstants.MaxFPS;
            LoadScene(_gameConstants.FirstLevelID);
        }

        public void LoadScene(int sceneID)
        {
            StartCoroutine(LoadSceneProgress(sceneID));
        }

        private IEnumerator LoadSceneProgress(int sceneID)
        {
            AsyncOperation loadSceneAsync = SceneManager.LoadSceneAsync(sceneID);
            _loadingScreen.Show();

            while (loadSceneAsync.isDone == false) 
            {
                _loadingScreen.UpdateLoadProgress(loadSceneAsync.progress);
                yield return null;
            }

            _loadingScreen.Hide();
        }
    }
}