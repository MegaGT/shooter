using UnityEngine;

namespace Game.Constants
{
    [CreateAssetMenu(menuName = "Game/Constants", fileName = "Constants")]
    public class GameConstants : ScriptableObject
    {
        [field: SerializeField] public int MaxFPS { get; private set; } = 120;
        [field: SerializeField] public int FirstLevelID { get; private set; }
    }
}